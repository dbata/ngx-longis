export const REGISTRAR_APP_LOCATIONS = [
  {
    'privilege': 'Location',
    'help': 'Enables student counselors feature',
    'target': {
        'url': '^/students/counselors'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student specialization features like selecting or validating specialization',
    'target': {
        'url': '^/students/specialization'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student courses features like course requirements overview, statistics etc',
    'target': {
        'url': '^/students/courses'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables course units feature',
    'target': {
        'url': '^/courses/units'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables course shared feature',
    'target': {
        'url': '^/courses/shared'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student theses features',
    'target': {
        'url': '^/students/theses'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student department features',
    'target': {
        'url': '^/students/department'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student semester features like calculating current semester',
    'target': {
        'url': '^/students/semester'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student semester features like calculating current semester',
    'target': {
        'url': '^/study-programs/studyLevel'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student internship features',
    'target': {
        'url': '^/internships'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student period registration features',
    'target': {
        'url': '^/registrations'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables study program groups features like managing of selecting stduy program groups',
    'target': {
        'url': '^/program-groups'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student scholarship features',
    'target': {
        'url': '^/scholarships'
    },
    'mask': 0
  },
    {
      'privilege': 'Location',
      'target': {
          'url': '^/auth/'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'target': {
          'url': '^/error'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'account': {
          'name': 'Administrators'
      },
      'target': {
          'url': '^/'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/departments/current/documents/series/\\d+/items/parent'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/candidate-students/list/*'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/candidate-students/upload-actions/list/*'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/settings/lists/CandidateSources/*'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/departments/current/restore/students/*'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/'
      },
      'mask': 1
    }
  ];
  